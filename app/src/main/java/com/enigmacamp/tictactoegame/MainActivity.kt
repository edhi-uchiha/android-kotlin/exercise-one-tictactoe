package com.enigmacamp.tictactoegame

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*
import kotlin.collections.ArrayList

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun onClickBtn(view : View){

        val selectedBtn = view as Button

        var cellId = 1
        when(selectedBtn.id){
            R.id.btn1 -> cellId = 1
            R.id.btn2 -> cellId = 2
            R.id.btn3 -> cellId = 3
            R.id.btn4 -> cellId = 4
            R.id.btn5 -> cellId = 5
            R.id.btn6 -> cellId = 6
            R.id.btn7 -> cellId = 7
            R.id.btn8 -> cellId = 8
            R.id.btn9 -> cellId = 9
        }

        Log.d("btnClick:", selectedBtn.id.toString())
        Log.d("btnClick: cellId:", cellId.toString())

        playGame(cellId, selectedBtn)
    }

    var activePlayer = 1

    var player1 = ArrayList<Int>()
    var player2 = ArrayList<Int>()

    fun playGame(cellId: Int, selectedBtn: Button){

        if(activePlayer == 1){
            selectedBtn.text = "X"
            selectedBtn.setBackgroundResource(R.color.purple)
            player1.add(cellId)
            activePlayer = 2
            autoPlay()
            selectedBtn.isEnabled = false

        } else {
            selectedBtn.text = "O"
            selectedBtn.setBackgroundResource(R.color.yellow)
            player2.add(cellId)
            activePlayer = 1
            selectedBtn.isEnabled = false

        }

        checkWinner()

    }

    fun checkWinner(){

        var winner = -1

        // row 1
        if(player1.contains(1) && player1.contains(2) && player1.contains(3)){
            winner = 1
        }

        if(player2.contains(1) && player2.contains(2) && player2.contains(3)){
            winner = 2
        }

        // row 1
        if(player1.contains(4) && player1.contains(5) && player1.contains(6)){
            winner = 1
        }

        if(player2.contains(4) && player2.contains(5) && player2.contains(6)){
            winner = 2
        }

        // row 3
        if(player1.contains(7) && player1.contains(8) && player1.contains(9)){
            winner = 1
        }

        if(player2.contains(7) && player2.contains(8) && player2.contains(9)){
            winner = 2
        }

        // col 1
        if(player1.contains(1) && player1.contains(4) && player1.contains(7)){
            winner = 1
        }

        if(player2.contains(1) && player2.contains(4) && player2.contains(7)){
            winner = 2
        }

        // col 1
        if(player1.contains(2) && player1.contains(5) && player1.contains(8)){
            winner = 1
        }

        if(player2.contains(2) && player2.contains(5) && player2.contains(8)){
            winner = 2
        }

        // col 3
        if(player1.contains(3) && player1.contains(6) && player1.contains(9)){
            winner = 1
        }

        if(player2.contains(3) && player2.contains(6) && player2.contains(9)){
            winner = 2
        }

        if(winner == 1){
            player1WinCounts++
            Toast.makeText(this, "Player 1 win the game.", Toast.LENGTH_LONG).show()
            restartGame()
        }else if ( winner == 2){
            player2WinCounts++
            Toast.makeText(this, "Player 2 win the game.", Toast.LENGTH_LONG).show()
            restartGame()
        }

    }

    fun autoPlay(){

        var emptyCells = ArrayList<Int>()

        for(cellId in 1..9){
            if( !(player1.contains(cellId) || player2.contains(cellId))){
                emptyCells.add(cellId)
            }
        }

        Log.d("EMPTY SIZE ", emptyCells.size.toString())
        if(emptyCells.size == 0 ){
            restartGame()
        } else {

            val r = Random()
            val ranIndex  = r.nextInt(emptyCells.size)
            val cellId = emptyCells[ranIndex]

            var selectedBtn: Button?
            selectedBtn = when(cellId){
                1 -> btn1
                2 -> btn2
                3 -> btn3
                4 -> btn4
                5 -> btn5
                6 -> btn6
                7 -> btn7
                8 -> btn8
                9 -> btn9
                else -> { btn1 }

            }

            playGame(cellId, selectedBtn)
        }

    }

    var player1WinCounts = 0
    var player2WinCounts = 0

    fun restartGame(){

        activePlayer = 1
        player1.clear()
        player2.clear()

        for(cellId in 1..9){

            var selectedBtn: Button? = when(cellId){
                1 -> btn1
                2 -> btn2
                3 -> btn3
                4 -> btn4
                5 -> btn5
                6 -> btn6
                7 -> btn7
                8 -> btn8
                9 -> btn9
                else -> { btn1 }

            }

            selectedBtn!!.text = ""
            selectedBtn!!.setBackgroundResource(R.color.button)
            selectedBtn!!.isEnabled = true
        }

        Toast.makeText(this, "Player1: $player1WinCounts, Player2: $player2WinCounts", Toast.LENGTH_LONG).show()
    }
}
